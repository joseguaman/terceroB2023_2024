/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package vista.listas;

import controlador.TDA.listas.LinkedList;
import controlador.llantas.LlantaController;
import controlador.llantas.MarcaController;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTree;

import vista.arreglos.util.UtilVista;
import vista.listas.modeloTabla.ModeloTablallanta;

/**
 *
 * @author sebastian
 */
public class FrmLLantas extends javax.swing.JDialog {

    private LlantaController lc = new LlantaController();
    private MarcaController mc = new MarcaController();
    private ModeloTablallanta mtll = new ModeloTablallanta();

    /**
     * Creates new form FrmLLantas
     */
    public FrmLLantas(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();

        limpiar();
    }

    private void ordenar() {
        String criterio = cbxcriterio.getSelectedItem().toString().toLowerCase();
        Integer ascdesc = cbxascdesc.getSelectedIndex();
        try {
            mtll.setLlantas(lc.ordenar(ascdesc, criterio, lc.getLlantas()));
            tbltabla.setModel(mtll);
            tbltabla.updateUI();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null,
                    ex.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            //Logger.getLogger(FrmLLantas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void buscar() {
        String criterio = cbxcriterio.getSelectedItem().toString().toLowerCase();

        try {
            if (criterio.equalsIgnoreCase("precio")
                    || criterio.equalsIgnoreCase("precio menor")
                    || criterio.equalsIgnoreCase("precio mayor")) {
                Double precio = Double.parseDouble(txtbusqueda.getText());
                if (criterio.equalsIgnoreCase("precio")) {

                } else if (criterio.equalsIgnoreCase("precio mayor")) {
                    mtll.setLlantas(lc.buscarPrecioMayores(lc.getLlantas(), "precio", precio));

                } else {
                    mtll.setLlantas(lc.buscarPrecioMenores(lc.getLlantas(), "precio", precio));

                }

            } else if(criterio.equalsIgnoreCase("marca")) {
                //TODO..
                mtll.setLlantas(lc.buscarMarca(lc.listAll(), "id_marca", UtilVista.getComboMarcasL(cbxmarcaB)));
            }
            tbltabla.setModel(mtll);
            tbltabla.updateUI();

        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null,
                    ex.getMessage(),
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
            //Logger.getLogger(FrmLLantas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void limpiar() {

        txtaro.setText("");
        txtcodigo.setText(lc.generatedCode());
        txtcodigo.setEnabled(false);
        txtdesc.setText("");
        txtprecio.setText("");
        txtpresion.setText("");
        txtstock.setText("");
        lc.setLlanta(null);
        lc.setLlantas(new LinkedList<>());
        cargarTabla();
        tbltabla.clearSelection();
        lc.setIndex(-1);
        try {
            UtilVista.cargarMarcaL(cbxmarca);
            UtilVista.cargarMarcaL(cbxmarcaB);
            cbxmarcaB.setVisible(false);
            txtbusqueda.setVisible(true);
        } catch (Exception e) {
        }
    }

    private void cargarTabla() {
        mtll.setLlantas(lc.getLlantas());
        tbltabla.setModel(mtll);
        tbltabla.updateUI();
    }

    private Boolean validar() {
        return !txtaro.getText().trim().isEmpty()
                && !txtcodigo.getText().trim().isEmpty()
                && !txtdesc.getText().trim().isEmpty()
                && !txtprecio.getText().trim().isEmpty()
                && !txtpresion.getText().trim().isEmpty()
                && !txtstock.getText().trim().isEmpty();
    }

    private void guardar() {
        System.out.println(UtilVista.getComboMarcasL(cbxmarca));
        if (validar()) {
            try {
                lc.getLlanta().setId_marca(UtilVista.getComboMarcasL(cbxmarca).getId());
                //System.out.println(cbxmarca.getSelectedItem());
                lc.getLlanta().setAro(Integer.parseInt(txtaro.getText()));
                lc.getLlanta().setCodigo(txtcodigo.getText());
                lc.getLlanta().setDescripcion(txtdesc.getText());
                lc.getLlanta().setPrecio(Double.parseDouble(txtprecio.getText()));
                lc.getLlanta().setPresion(Double.parseDouble(txtpresion.getText()));
                lc.getLlanta().setStock(Integer.parseInt(txtstock.getText()));
                //GUARDAR
                if (lc.getLlanta().getId() == null) {
                    if (lc.save()) {
                        limpiar();
                        JOptionPane.showMessageDialog(null,
                                "Se ha guardado correctamente",
                                "OK",
                                JOptionPane.INFORMATION_MESSAGE);

                    } else {
                        JOptionPane.showMessageDialog(null,
                                "No se pudo guardar",
                                "Error",
                                JOptionPane.ERROR_MESSAGE);
                    }
                } else {
                    if (lc.update(lc.getIndex())) {
                        limpiar();
                        JOptionPane.showMessageDialog(null,
                                "Se ha editado correctamente",
                                "OK",
                                JOptionPane.INFORMATION_MESSAGE);

                    } else {
                        JOptionPane.showMessageDialog(null,
                                "No se pudo editar",
                                "Error",
                                JOptionPane.ERROR_MESSAGE);
                    }
                }

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null,
                        e.getMessage(),
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(null,
                    "Llene todos los campos",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

    private void cargarVista() {
        //int fila = tbltabla.getSelectedRow();
        lc.setIndex(tbltabla.getSelectedRow());
        if (lc.getIndex().intValue() < 0) {

            JOptionPane.showMessageDialog(null,
                    "Seleccione una fila",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
        } else {
            try {
                lc.setLlanta(mtll.getLlantas().get(lc.getIndex()));
                txtaro.setText(lc.getLlanta().getAro().toString());
                txtcodigo.setText(lc.getLlanta().getCodigo());
                txtdesc.setText(lc.getLlanta().getDescripcion());
                txtprecio.setText(lc.getLlanta().getPrecio().toString());
                txtpresion.setText(lc.getLlanta().getPresion().toString());
                txtstock.setText(lc.getLlanta().getStock().toString());
                cbxmarca.setSelectedIndex(lc.getLlanta().getId_marca() - 1);
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null,
                        e.getMessage(),
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        cbxmarca = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        txtcodigo = new javax.swing.JTextField();
        txtstock = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtprecio = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtaro = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtpresion = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtdesc = new javax.swing.JTextArea();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbltabla = new javax.swing.JTable();
        jButton3 = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        txtbusqueda = new javax.swing.JTextField();
        cbxcriterio = new javax.swing.JComboBox<>();
        cbxascdesc = new javax.swing.JComboBox<>();
        jButton4 = new javax.swing.JButton();
        cbxmarcaB = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(null);

        jPanel1.setBorder(javax.swing.BorderFactory.createCompoundBorder());
        jPanel1.setLayout(null);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED), "Informacion"));
        jPanel2.setLayout(null);

        jLabel1.setFont(new java.awt.Font("sansserif", 1, 13)); // NOI18N
        jLabel1.setText("Codigo");
        jPanel2.add(jLabel1);
        jLabel1.setBounds(350, 40, 60, 18);

        cbxmarca.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jPanel2.add(cbxmarca);
        cbxmarca.setBounds(100, 40, 220, 25);

        jLabel2.setFont(new java.awt.Font("sansserif", 1, 13)); // NOI18N
        jLabel2.setText("Marca");
        jPanel2.add(jLabel2);
        jLabel2.setBounds(30, 40, 60, 18);
        jPanel2.add(txtcodigo);
        txtcodigo.setBounds(420, 40, 190, 25);
        jPanel2.add(txtstock);
        txtstock.setBounds(90, 90, 220, 25);

        jLabel3.setFont(new java.awt.Font("sansserif", 1, 13)); // NOI18N
        jLabel3.setText("Stock");
        jPanel2.add(jLabel3);
        jLabel3.setBounds(20, 90, 60, 18);

        jLabel4.setFont(new java.awt.Font("sansserif", 1, 13)); // NOI18N
        jLabel4.setText("Precio");
        jPanel2.add(jLabel4);
        jLabel4.setBounds(350, 80, 60, 18);
        jPanel2.add(txtprecio);
        txtprecio.setBounds(420, 80, 190, 25);

        jLabel5.setFont(new java.awt.Font("sansserif", 1, 13)); // NOI18N
        jLabel5.setText("Descripcion");
        jPanel2.add(jLabel5);
        jLabel5.setBounds(20, 180, 90, 18);
        jPanel2.add(txtaro);
        txtaro.setBounds(90, 130, 220, 25);

        jLabel6.setFont(new java.awt.Font("sansserif", 1, 13)); // NOI18N
        jLabel6.setText("Presion");
        jPanel2.add(jLabel6);
        jLabel6.setBounds(350, 120, 60, 18);
        jPanel2.add(txtpresion);
        txtpresion.setBounds(420, 120, 190, 25);

        jLabel7.setFont(new java.awt.Font("sansserif", 1, 13)); // NOI18N
        jLabel7.setText("Aro");
        jPanel2.add(jLabel7);
        jLabel7.setBounds(20, 130, 60, 18);

        txtdesc.setColumns(20);
        txtdesc.setLineWrap(true);
        txtdesc.setRows(5);
        txtdesc.setWrapStyleWord(true);
        jScrollPane1.setViewportView(txtdesc);

        jPanel2.add(jScrollPane1);
        jScrollPane1.setBounds(110, 180, 490, 70);

        jButton1.setText("CANCELAR");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton1);
        jButton1.setBounds(360, 260, 140, 25);

        jButton2.setText("GUARDAR");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton2);
        jButton2.setBounds(150, 260, 110, 25);

        jPanel1.add(jPanel2);
        jPanel2.setBounds(10, 10, 640, 300);

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED), "Lista de productos"));
        jPanel3.setLayout(null);

        tbltabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tbltabla);

        jPanel3.add(jScrollPane2);
        jScrollPane2.setBounds(20, 90, 480, 100);

        jButton3.setText("Seleccionar");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton3);
        jButton3.setBounds(510, 140, 110, 25);

        jLabel8.setFont(new java.awt.Font("sansserif", 1, 13)); // NOI18N
        jLabel8.setText("Criterio");
        jPanel3.add(jLabel8);
        jLabel8.setBounds(300, 40, 70, 19);

        jLabel9.setFont(new java.awt.Font("sansserif", 1, 13)); // NOI18N
        jLabel9.setText("Texto");
        jPanel3.add(jLabel9);
        jLabel9.setBounds(20, 40, 38, 18);
        jPanel3.add(txtbusqueda);
        txtbusqueda.setBounds(80, 40, 200, 25);

        cbxcriterio.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "CODIGO", "STOCK", "ARO", "PRECIO", "PRECIO MAYOR", "PRECIO MENOR", "MARCA" }));
        cbxcriterio.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbxcriterioItemStateChanged(evt);
            }
        });
        jPanel3.add(cbxcriterio);
        cbxcriterio.setBounds(370, 40, 120, 25);

        cbxascdesc.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "ASCENDENTE", "DESCENDENTE" }));
        cbxascdesc.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbxascdescItemStateChanged(evt);
            }
        });
        cbxascdesc.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                cbxascdescInputMethodTextChanged(evt);
            }
        });
        jPanel3.add(cbxascdesc);
        cbxascdesc.setBounds(510, 40, 100, 25);

        jButton4.setText("BUSCAR");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton4);
        jButton4.setBounds(510, 100, 110, 25);

        cbxmarcaB.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cbxmarcaB.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbxmarcaBItemStateChanged(evt);
            }
        });
        jPanel3.add(cbxmarcaB);
        cbxmarcaB.setBounds(80, 40, 210, 25);

        jPanel1.add(jPanel3);
        jPanel3.setBounds(10, 320, 640, 200);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 660, 520);

        setSize(new java.awt.Dimension(664, 555));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        guardar();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        cargarVista();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        limpiar();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void cbxcriterioItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbxcriterioItemStateChanged
        // TODO add your handling code here:
        //ordenar();
        if(evt.getItem().toString().equalsIgnoreCase("MARCA")) {
            txtbusqueda.setVisible(false);
            cbxmarcaB.setVisible(true);
        } else {
            txtbusqueda.setVisible(true);
            cbxmarcaB.setVisible(false);
        }
    }//GEN-LAST:event_cbxcriterioItemStateChanged

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
        buscar();
    }//GEN-LAST:event_jButton4ActionPerformed

    private void cbxascdescInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_cbxascdescInputMethodTextChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cbxascdescInputMethodTextChanged

    private void cbxascdescItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbxascdescItemStateChanged
        // TODO add your handling code here:
        //ordenar();
    }//GEN-LAST:event_cbxascdescItemStateChanged

    private void cbxmarcaBItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbxmarcaBItemStateChanged
        // TODO add your handling code here:
        buscar();
    }//GEN-LAST:event_cbxmarcaBItemStateChanged

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmLLantas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmLLantas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmLLantas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmLLantas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                FrmLLantas dialog = new FrmLLantas(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> cbxascdesc;
    private javax.swing.JComboBox<String> cbxcriterio;
    private javax.swing.JComboBox<String> cbxmarca;
    private javax.swing.JComboBox<String> cbxmarcaB;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tbltabla;
    private javax.swing.JTextField txtaro;
    private javax.swing.JTextField txtbusqueda;
    private javax.swing.JTextField txtcodigo;
    private javax.swing.JTextArea txtdesc;
    private javax.swing.JTextField txtprecio;
    private javax.swing.JTextField txtpresion;
    private javax.swing.JTextField txtstock;
    // End of variables declaration//GEN-END:variables
}
