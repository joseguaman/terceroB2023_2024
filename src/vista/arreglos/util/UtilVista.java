/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vista.arreglos.util;

import controlador.MarcaController;
import controlador.TDA.listas.LinkedList;
import controlador.TDA.listas.exception.VacioException;
import javax.swing.JComboBox;
import modelo.Marca;

/**
 *
 * @author sebastian
 */
public class UtilVista {
    public static void cargarMarca(JComboBox cbxmarca) {
        MarcaController mc = new MarcaController();
        cbxmarca.removeAllItems();
        for(int i = 0; i < mc.getMarcas().length; i++) {
            cbxmarca.addItem(mc.getMarcas()[i]);
        }
    }
    public static Marca getComboMarcas(JComboBox cbx) {
        return (Marca)cbx.getSelectedItem();
    }
    
    public static void cargarMarcaL(JComboBox cbxmarca) throws VacioException, Exception {
        controlador.llantas.MarcaController mc = new controlador.llantas.MarcaController();
        cbxmarca.removeAllItems();
        LinkedList<Marca> lista = mc.ordenar(0, "nombre", mc.listAll());
        
        for(int i = 0; i < lista.getSize(); i++) {
            cbxmarca.addItem(lista.get(i));
        }
    }
    public static Marca getComboMarcasL(JComboBox cbx) {
        return (Marca)cbx.getSelectedItem();
    }
}








