/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vista.arreglos.tablas;

import javax.swing.table.AbstractTableModel;
import modelo.Llanta;

/**
 *
 * @author sebastian
 */
public class ModeloTablallanta extends AbstractTableModel {
    private Llanta[] llantas;

    public Llanta[] getLlantas() {
        return llantas;
    }

    public void setLlantas(Llanta[] llantas) {
        this.llantas = llantas;
    }
    
    
    
    @Override
    public int getRowCount() {
        // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        return llantas.length;
    }

    @Override
    public int getColumnCount() {
        return 4; // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Object getValueAt(int i, int i1) {
        Llanta llanta = llantas[i];
        // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        switch (i1) {
            case 0:
                return (llanta != null) ? llanta.getDescripcion() : "";
            case 1:
                return (llanta != null) ? llanta.getAro(): "";
            case 2:
                return (llanta != null) ? llanta.getCodigo(): "";
            case 3:
                return (llanta != null) ? "$"+llanta.getPrecio(): "";
            default:
                return null;
        }
    }

    @Override
    public String getColumnName(int column) {
         // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
         switch (column) {
            case 0:
                return "Descripcion";
            case 1:
                return "Aro";
            case 2:
                return "Codigo";
            case 3:
                return "Precio";
            default:
                return null;
        }
    }
    
    
    
}
