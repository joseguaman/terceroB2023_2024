/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo.catalogo;

/**
 *
 * @author sebastian
 */
public enum TipoPago {
    EFECTIVO, TARJETA_CREDITO, DIFERIDO;
}
