/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;

/**
 *
 * @author sebastian
 */
public class Llanta {
    private Integer id;
    private Double precio;
    private String codigo;
    private Integer aro;
    private Double presion;
    private Integer stock;
    private String descripcion;
    private Integer id_marca;

   public Boolean comparar(Llanta c, String field, Integer type) {
        switch (type) {
            case 1:
                if (field.equalsIgnoreCase("codigo")) {
                    return getCodigo().compareTo(c.getCodigo()) > 0;
                } else if (field.equalsIgnoreCase("descripcion")) {
                    return getDescripcion().compareTo(c.getDescripcion()) > 0;
                }
                else if (field.equalsIgnoreCase("id")) {
                    return getId().intValue() > c.getId().intValue();
                } 
                else if (field.equalsIgnoreCase("precio")) {
                    return getPrecio().doubleValue()> c.getPrecio().doubleValue();
                } 
                else if (field.equalsIgnoreCase("presion")) {
                    return getPresion().doubleValue()> c.getPresion().doubleValue();
                }
                else if (field.equalsIgnoreCase("aro")) {
                    return getAro().intValue()> c.getAro().intValue();
                }
                else if (field.equalsIgnoreCase("stock")) {
                    return getStock().intValue()> c.getStock().intValue();
                }
                else if (field.equalsIgnoreCase("id_marca")) {
                    return getId_marca().intValue()> c.getId_marca().intValue();
                }
                System.out.println("x");
                //break;
            case 0:
                if (field.equalsIgnoreCase("codigo")) {
                    return getCodigo().compareTo(c.getCodigo()) < 0;
                } else if (field.equalsIgnoreCase("descripcion")) {
                    return getDescripcion().compareTo(c.getDescripcion()) > 0;
                }
                else if (field.equalsIgnoreCase("id")) {
                    return getId().intValue() < c.getId().intValue();
                } 
                else if (field.equalsIgnoreCase("precio")) {
                    return getPrecio().doubleValue() < c.getPrecio().doubleValue();
                } 
                else if (field.equalsIgnoreCase("presion")) {
                    return getPresion().doubleValue() < c.getPresion().doubleValue();
                }
                else if (field.equalsIgnoreCase("aro")) {
                    return getAro().intValue() < c.getAro().intValue();
                }
                else if (field.equalsIgnoreCase("stock")) {
                    return getStock().intValue() < c.getStock().intValue();
                }
                else if (field.equalsIgnoreCase("id_marca")) {
                    return getId_marca().intValue() < c.getId_marca().intValue();
                }
                //break;
            default:
                return null;
        }

    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Integer getAro() {
        return aro;
    }

    public void setAro(Integer aro) {
        this.aro = aro;
    }

    public Double getPresion() {
        return presion;
    }

    public void setPresion(Double presion) {
        this.presion = presion;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getId_marca() {
        return id_marca;
    }

    public void setId_marca(Integer id_marca) {
        this.id_marca = id_marca;
    }
    
    
}
