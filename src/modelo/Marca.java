/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;

import java.io.Serializable;

/**
 *
 * @author sebastian
 */
public class Marca {

    private Integer id;
    private String nombre;
    private Boolean estado;

    public Marca(Integer id, String nombre, Boolean estado) {
        this.id = id;
        this.nombre = nombre;
        this.estado = estado;
    }

    public Marca() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    /**
     * Compara dos objetos Marcas
     *
     * @param c otro objeto Marca
     * @param field nombre del Atributo
     * @param type Si es ascendente = 0 o descendente = 1
     * @return Boolean true si cumple con la comparacion
     */
    public Boolean comparar(Marca c, String field, Integer type) {
        switch (type) {
            case 1:
                if (field.equalsIgnoreCase("nombre")) {
                    return getNombre().compareTo(c.getNombre()) > 0;
                }else if (field.equalsIgnoreCase("id")) {
                    return getId().intValue() > c.getId().intValue();
                }  
                System.out.println("x");
                //break;
            case 0:
                if (field.equalsIgnoreCase("nombre")) {
                    return getNombre().compareTo(c.getNombre()) < 0;
                }else if (field.equalsIgnoreCase("id")) {
                    return getId().intValue() < c.getId().intValue();
                }   
                System.out.println("y");
                //break;
            default:
                return null;
        }

    }

    @Override
    public String toString() {
        return id + "--" + nombre; // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

}
