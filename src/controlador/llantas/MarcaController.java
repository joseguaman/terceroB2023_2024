/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.llantas;

import com.thoughtworks.xstream.XStream;
import controlador.TDA.listas.LinkedList;
import controlador.TDA.listas.exception.VacioException;
import controlador.llantas.dao.Connection;
import controlador.llantas.dao.DataAccessObject;
import controlador.util.Utilidades;
import java.io.FileWriter;
import java.lang.reflect.Field;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Marca;

/**
 *
 * @author sebastian
 */
public class MarcaController extends DataAccessObject<Marca> {

    private LinkedList<Marca> marcas = new LinkedList<>();
    private Marca marca;

    public MarcaController() {
        super(Marca.class);
    }

    public Marca getMarca() {
        if (marca == null) {
            marca = new Marca();
        }
        return marca;
    }

    public void setMarca(Marca marca) {
        this.marca = marca;
    }

    public LinkedList<Marca> getMarcas() {
        if (marcas.isEmpty()) {
            marcas = this.listAll();
        }
        return marcas;
    }

    public void setMarcas(LinkedList<Marca> marcas) {
        this.marcas = marcas;
    }

    public Boolean save() {
        this.marca.setId(generated_id());
        return this.save(marca);
    }

    public LinkedList<Marca> ordenar(Integer type) throws VacioException {
        getMarcas();
        Integer n = marcas.getSize();
        Marca[] m = marcas.toArray();
        //Double   Integer  Short 
        for (int i = 0; i < n - 1; i++) {
            int k = i;
            Marca t = m[i];//marcas.get(i);
            for (int j = i + 1; j < n; j++) {
                Marca mj = m[j];//marcas.get(j);
                if (type == 0) {
                    if (mj.getNombre().compareTo(t.getNombre()) < 0) {
                        t = mj;
                        k = j;
                    }
                } else {
                    if (mj.getNombre().compareTo(t.getNombre()) > 0) {
                        t = mj;
                        k = j;
                    }
                }
            }
            m[k] = m[i];
            m[i] = t;
        }
        marcas = marcas.toList(m);

        return marcas;
    }
    public LinkedList<Marca> ordenar(Integer type, String field, LinkedList<Marca> lista) throws VacioException, Exception {
       Integer n = lista.getSize();
        Marca[] m = lista.toArray();
        Field faux = Utilidades.getField(Marca.class, field);
        if (faux != null) {
            for (int i = 0; i < n - 1; i++) {
                int k = i;
                Marca t = m[i];//marcas.get(i);
                for (int j = i + 1; j < n; j++) {
                    Marca mj = m[j];//marcas.get(j);                   
                    if (mj.comparar(t, field, type)) {
                        t = mj;
                        k = j;
                    }
                }
                m[k] = m[i];
                m[i] = t;
            }
            lista = lista.toList(m);
        } else
            throw new Exception("No existe ese criterio de busqueda");

        return lista;
    }
    
    public LinkedList<Marca> buscar(LinkedList<Marca> lista, String text) throws Exception {
        LinkedList<Marca> lo = this.ordenar(0, "nombre", lista);
        Marca[] m = lo.toArray();
        LinkedList<Marca> result = new LinkedList<>();
        for(int i = 0; i < lo.getSize(); i++) {
            if(m[i].getNombre().toLowerCase().contains(text.toLowerCase())) {
                result.add(m[i]);
            }
        }
        return result;
    }

    public static void main(String[] args) {
        MarcaController mc = new MarcaController();
        try {
            System.out.println(mc.buscar(mc.listAll(), "bridegstone").print());;
            
            /*mc.getMarca().setId(1);
            mc.getMarca().setNombre("Bridegstone");
            mc.getMarca().setEstado(true);
            mc.save();
            mc.setMarca(null);
            mc.getMarca().setId(2);
            mc.getMarca().setNombre("Triangule");
            mc.getMarca().setEstado(true);
            mc.save();
            mc.setMarca(null);
            mc.getMarca().setId(3);
            mc.getMarca().setNombre("General Tire");
            mc.getMarca().setEstado(true);
            mc.save();
            mc.setMarca(null);
            mc.getMarca().setId(4);
            mc.getMarca().setNombre("Ferrari");
            mc.getMarca().setEstado(true);
            mc.save();
            mc.setMarca(null);*/
           // System.out.println(mc.ordenar(0, "nombre", mc.getMarcas()).print());
            /*mc.getMarca().setId(10);
            mc.getMarca().setNombre("Test");
            mc.getMarca().setEstado(true);
            Marca[] marcas = mc.listAll().toArray();
            for (Marca m : marcas) {
                Object resp = Utilidades.getData(m, "estado");
                System.out.println(resp);
            }*/
            //Field id = Utilidades.getField(Marca.class, "estado");
//            Object resp = Utilidades.getData(mc.getMarca(), "nombre");
//            if(resp != null) {
//                System.out.println("OK "+resp.toString());
//            } else System.out.println("NO hay como");

        } catch (Exception ex) {
            System.out.println("Error "+ex.getMessage());
            //Logger.getLogger(MarcaController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
