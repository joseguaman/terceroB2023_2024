/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.llantas;

import controlador.TDA.listas.LinkedList;
import controlador.TDA.listas.exception.VacioException;
import controlador.llantas.dao.DataAccessObject;
import controlador.util.Utilidades;
import java.lang.reflect.Field;
import modelo.Llanta;
import modelo.Marca;

/**
 *
 * @author sebastian
 */
public class LlantaController extends DataAccessObject<Llanta>{
    
    private Llanta llanta = new Llanta();
    private LinkedList<Llanta> llantas = new LinkedList<>();
    private Integer index = -1;

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }
    
    public LlantaController() {
        super(Llanta.class);
    }    

    public LinkedList<Llanta> getLlantas() {
        if(llantas.isEmpty())
            llantas = listAll();
        return llantas;
    }

    public void setLlantas(LinkedList<Llanta> llantas) {
        this.llantas = llantas;
    }

    public Llanta getLlanta() {
        if(llanta == null)
            llanta = new Llanta();
        return llanta;
    }

    public void setLlanta(Llanta llanta) {
        this.llanta = llanta;
    }
    
    public Boolean save() {
        llanta.setId(generated_id());
        return save(llanta);
    }
    
    public Boolean update(Integer index) {
        //llanta.setId(generated_id());
        return update(llanta, index);
    }
    
    public String generatedCode() {
        //lista ---> 21   ---- 0000000021
        StringBuilder code = new StringBuilder();
        Integer length = listAll().getSize() + 1;
        Integer pos = length.toString().length();
        for(int i = 0; i < (10 - pos); i++) {
            code.append("0");
        }
        code.append(length.toString());
        return code.toString();
    }
    
    public LinkedList<Llanta> ordenar(Integer type, String field, LinkedList<Llanta> lista) throws VacioException, Exception {
        //getMarcas();
        Integer n = lista.getSize();
        Llanta[] m = lista.toArray();
        Field faux = Utilidades.getField(Llanta.class, field);
        if (faux != null) {
            for (int i = 0; i < n - 1; i++) {
                int k = i;
                Llanta t = m[i];//marcas.get(i);
                for (int j = i + 1; j < n; j++) {
                    Llanta mj = m[j];//marcas.get(j);                   
                    if (mj.comparar(t, field, type)) {
                        t = mj;
                        k = j;
                    }
                }
                m[k] = m[i];
                m[i] = t;
            }
            lista = lista.toList(m);
        } else
            throw new Exception("No existe ese criterio de busqueda");

        return lista;
    }
    
    public LinkedList<Llanta> buscarPrecioMenores(LinkedList<Llanta> lista, String text, Double precio) throws Exception {
        LinkedList<Llanta> lo = this.ordenar(0, text, lista);
        Llanta[] m = lo.toArray();
        LinkedList<Llanta> result = new LinkedList<>();        
        for(int i = 0; i < lo.getSize(); i++) {
            System.out.println(m[i].getPrecio()+" < "+ precio.doubleValue());
            if(m[i].getPrecio().doubleValue() <= precio.doubleValue()) {
                result.add(m[i]);
            }
        }
        return result;
    }
    
    public LinkedList<Llanta> buscarMarca(LinkedList<Llanta> lista, String text, Marca marca) throws Exception {
        LinkedList<Llanta> lo = this.ordenar(0, text, lista);
        Llanta[] m = lo.toArray();
        LinkedList<Llanta> result = new LinkedList<>();   
        //System.out.println(marca);
        for(int i = 0; i < lo.getSize(); i++) {
            
            if(m[i].getId_marca().intValue() == marca.getId().intValue()) {
                result.add(m[i]);
            }
        }
        return result;
    }
    
    public LinkedList<Llanta> buscarPrecioMayores(LinkedList<Llanta> lista, String text, Double precio) throws Exception {
        LinkedList<Llanta> lo = this.ordenar(0, text, lista);
        Llanta[] m = lo.toArray();
        LinkedList<Llanta> result = new LinkedList<>();        
        for(int i = 0; i < lo.getSize(); i++) {
            System.out.println(m[i].getPrecio()+" < "+ precio.doubleValue());
            if(m[i].getPrecio().doubleValue() >= precio.doubleValue()) {
                result.add(m[i]);
            }
        }
        return result;
    }
    
}
