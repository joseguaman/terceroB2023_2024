/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador;

import modelo.Llanta;

/**
 *
 * @author sebastian
 */
public class LlantaController {
    private Llanta [] llantas;
    private Llanta llanta = new Llanta();
    
    public LlantaController(Integer n) {
        this.llantas = new Llanta[n];
    }

    public Llanta getLlanta() {
        if(llanta == null)
            llanta = new Llanta();
        return llanta;
    }

    public void setLlanta(Llanta llanta) {
        this.llanta = llanta;
    }
    
    public Llanta[] getLlantas() {
        return llantas;
    }

    public void setLlantas(Llanta[] llantas) {
        this.llantas = llantas;
    }
    
    public Boolean guardar() {
        Integer pos = pos_verificar();
        if(pos > -1) {
            llanta.setId(generar_id());
            llantas[pos_verificar()] = llanta;            
            return true;
        }
        return false;
    }
    
    public void imprimir() {
        System.out.println("");
        for(int i = 0; i < llantas.length; i++) {
            if(llantas[i] != null)
                System.out.print("id: "+llantas[i].getId()+ " descripcion: "+llantas[i].getDescripcion());
        }
        System.out.println("");
    }
    
    private Integer pos_verificar() {
        //Boolean band = false;
        for(int i = 0; i < llantas.length; i++) {
            if(llantas[i] == null) {
                return i;
            }
        }
        return -1;
    }
    
    private Integer generar_id() {
        return llantas.length + 1;
    }
    
}













