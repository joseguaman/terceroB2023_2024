/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador;

import modelo.Marca;

/**
 *
 * @author sebastian
 */
public class MarcaController {
    private Marca[] marcas;

    public MarcaController() {
        marcas = new Marca[6];
        marcas[0] = new Marca(1,"Toyota", true);
        marcas[1] = new Marca(2,"Nissan", true);
        marcas[2] = new Marca(3,"Chevrolet", true);
        marcas[3] = new Marca(4,"Hyundai", true);
        marcas[4] = new Marca(5,"Audi", true);
        marcas[5] = new Marca(6,"Kia", true);        
    }

    public Marca[] getMarcas() {
        return marcas;
    }

    public void setMarcas(Marca[] marcas) {
        this.marcas = marcas;
    }
    
}









