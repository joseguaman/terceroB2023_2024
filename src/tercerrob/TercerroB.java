/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package tercerrob;


import controlador.TDA.grafos.GradoEtiquetadoNoDirigido;
import controlador.TDA.listas.Queque;
import controlador.TDA.listas.Stack;
import controlador.TDA.listas.exception.LlenoException;
import controlador.TDA.listas.exception.VacioException;
import controlador.llantas.MarcaController;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Marca;

/**
 *
 * @author sebastian
 */
public class TercerroB {
//    private String[] signosCierre = {"{ }","( )","[ ]","< >"};
//    //< >
//    //private String[] signosApertura = {"{","(","[","<"};
//    private Object[] obtenerCaracter(Character a) {
//        Object [] resp = new Object[2];//0  el caracter   1  si es de apertura o cierre
//        for(String b:signosCierre) {
//            String[] aux = b.split(" ");
//            
//            if(aux[0].charAt(0) == a.charValue()) {
//                resp[0] = a;
//                resp[1] = 1;
//                break;                
//            } else if(aux[1].charAt(0) == a.charValue()) {                
//                resp[0] = aux[0].charAt(0);
//                resp[1] = 2;                
//                break;
//            } else {
//                resp[0] = a;
//                resp[1] = 0;
//            }            
//        }
//        return resp;
//    }
//    public Boolean verificar(String texto) throws LlenoException, VacioException {
//        Stack<Character> pila = new Stack<>(10);
//        Boolean band = false; //[()]
//        Integer estado = 0;//1  apertura //2 cierre
//        for(Character a:texto.toCharArray()) {                        
//            Object[] datos = obtenerCaracter(a);
//            
//            if(Integer.parseInt(datos[1].toString()) == 1) {
//                pila.push(a);
//            
//            } else if(Integer.parseInt(datos[1].toString()) == 2) {
//                Character aper = datos[0].toString().charAt(0);
//                Character popC = pila.pop();
//            
//                if(aper.charValue() != popC.charValue()) {
//                    pila.push(aper);
//                    break;
//                }
//            } 
//        }
//        if(pila.getSize() == 0)
//            band = true;
//        return band;
//    }

    public static void imprimir(Integer[] m) {
        System.out.println("********");
        for (int i = 0; i < m.length; i++) {
            System.out.print(m[i] + "\t");
        }
        System.out.println("********");
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        MarcaController mc = new MarcaController();
        GradoEtiquetadoNoDirigido<Marca> gend = new GradoEtiquetadoNoDirigido<>(mc.getMarcas().getSize(), Marca.class);
        Marca[] arreglo = mc.getMarcas().toArray();
        for(int i = 0; i < arreglo.length; i++) {
            gend.etiquetarVertice((i+1), arreglo[i]);
        }
        try {
            gend.insertarAristaE(mc.buscar(mc.getMarcas(), "Bridegstone").getFirst(), mc.buscar(mc.getMarcas(), "Ferrari").getFirst());
        } catch (Exception ex) {
            System.out.println("Error "+ex);
        }
        System.out.println(gend.toString());
// TODO code application logic here
//        try {
//            String a = "[(4+5)*9]";
//            //[
//            //)
//            TercerroB tb = new TercerroB();
//            if(tb.verificar(a)) {
//                System.out.println("MUY BIEN!");
//            } else {
//                System.out.println("NO ES VALIDO!");
//            }
//        } catch (Exception e) {
//            System.out.println("ERROR "+e);
//        }
//        Integer[] m = new Integer[5000];
//        Integer[] sac = new Integer[5000];
//        Integer[] in = new Integer[5000];
//        Integer[] se = new Integer[5000];
//        for (int i = 0; i < m.length; i++) {
//            Integer aux = (int) (Math.random() * 100);
//            m[i] = aux;
//            in[i] = aux;
//            se[i] = aux;
//            sac[i] = aux;
//        }
//        TercerroB.imprimir(m);
//        Integer intercambios = 0;
//        //burbuja
//        for (int i = 1; i < m.length - 1; i++) {
//            for (int j = m.length - 1; j > 0; j--) {
//                if (m[j - 1] > m[j]) {
//                    Integer aux = m[j - 1];
//                    m[j - 1] = m[j];
//                    m[j] = aux;
//                    intercambios++;
//                }
//            }
//        }
//        TercerroB.imprimir(m);
//        System.out.println("Intrercambios burbuja " + intercambios);
//        //insercion
//        System.out.println("INSERSION");
//        TercerroB.imprimir(in);
//        Integer intercambioI = 0;
//        Integer n = in.length;
//        for (int i = 1; i < n; i++) {
//            int j = i - 1;
//            Integer t = in[i];
//            while (j >= 0 && t < in[j]) {
//                in[j + 1] = in[j];
//                j = j - 1;
//                intercambioI++;
//            }
//            in[j + 1] = t;
//        }
//        TercerroB.imprimir(in);
//        System.out.println("Intrercambios insercion " + intercambioI);
//
//        //SELECCION
//        System.out.println("SELECCION");
//        TercerroB.imprimir(se);
//        Integer intercambioSe = 0;
//
//        for (int i = 0; i < n - 1; i++) {
//            int k = i;
//            int t = se[i];
//            for (int j = i + 1; j < n; j++) {
//                if (se[j] < t) {
//                    t = se[j];
//                    k = j;
//                    intercambioSe++;
//                }
//            }
//            se[k] = se[i];
//            se[i] = t;
//        }
//
//        TercerroB.imprimir(in);
//        System.out.println("Intrercambios seleccion " + intercambioSe);
//        
//        
//        //sacudida
//        System.out.println("SACUDIDA");
//        TercerroB.imprimir(sac);
//        Integer intercambioSac = 0;
//        int i,j,izq,der,aux = 0;
//        izq = 1;
//        der = n - 1;
//        j = n - 1;
//        do{
//            for(i = der;i >=izq; i--) {
//                if(sac[i-1]>sac[i]) {
//                    aux = sac[i];
//                    sac[i] = sac[i-1];
//                    sac[i-1] = aux;
//                    j = i;
//                    intercambioSac++;
//                }
//            }
//            izq = j+1;
//            for(i = izq;i <=der; i++) {
//                if(sac[i-1]>sac[i]) {
//                    aux = sac[i];
//                    sac[i] = sac[i-1];
//                    sac[i-1] = aux;
//                    j = i;
//                    intercambioSac++;
//                }
//            }
//            der = j-1;
//        }while (izq <= der);
//
//        TercerroB.imprimir(sac);
//        System.out.println("Intrercambios seleccion " + intercambioSac);
    }
    // 0   1  2   3    4   5    6
    //98  45  1   5    9   0   -1
    //pos = 4
    //pos = 5
}
